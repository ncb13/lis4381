# LIS4381

## Nikhil Bosshardt

### Assignment 4 Requirments:

* Screen shots of modified php files
* Screen shots of new index file
* Local and bitbucket links

***

*1. Screen Shot of modified PhP index:*

![Modified index](images/ss1.PNG)

***

*2. Screen Shot of Client-side Input Validation with PhP:*

![Input Validation](images/ss2.PNG)

***

*Links (REMEBER TO START AMPPS):*
** [Local repo](http://localhost/repos/a4/)**


