# LIS4381

## Nikhil Bosshardt

### Project 1 Requirments:

* Screenshot of Business Card app running 1st activity
* Screenshot of Business Card app running 2nd activity


*1. Screen Shot of main activity:*

![act1](images/ss1.PNG)

*2. Screenshot of second activity*

![act2](images/ss2.PNG)
