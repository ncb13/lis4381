# LIS 4381 - Mobile App Dev

## Nikhil Bosshardt

### Coursework Links

*[A1 README!](a1/README.md)*

* Configuring git and bitbucket
* Repository setup
* Installing AMPPS
* JAVA install and simple app
* Android Studio and simple app
* Repo links!

*[A2 README!](a2/README.md)*

* Simple recipe app with screenshots.
* Two activites and buttons.

*[A3 README!](a3/README.md)*

* Petstore database with MWB and SQL links
* Concert App Running with Screenshots!

*[P1 README!](p1/README.md)*

* Business Card activity with custom colors
* Background, shadows, outlines, and gradients all included

*[A4 README!](a4/README.md)*

* Modified PhP index file running on AMPPS
* Modified PhP file with input validation running on AMPPS
* Screen shots included!

*[A5 README!](a5/README.md)*

* Modified PhP index file running on AMPPS with sql table
* Modified PhP form to add data to sql database (error screen included)
* Screen shots included!

*[P2 README!](p2/README.md)*

* Modified PhP index file running on AMPPS with sql table
* Modified PhP form to add/edit/delete data on sql database (error screen included)
* RSS Feed