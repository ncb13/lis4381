<!DOCTYPE HTML>
<HTML>
<head>
<meta charset="utf-8">
<title>P2 RSS Feed</title>
<!-- Include FontAwesome CSS to use feedback icons provided by FontAwesome -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.4.0/css/font-awesome.min.css">

<!-- Bootstrap for responsive, mobile-first design. -->
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css">
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">

<!-- Starter template for your own custom styling. -->
<link href="css/starter-template.css" rel="stylesheet">

<!-- jQuery DataTables: http://www.datatables.net/ //-->
<link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/1.10.9/css/jquery.dataTables.min.css"/>
<link rel="stylesheet" type=""text/css" href="//cdn.datatables.net/responsive/1.0.7/css/dataTables.responsive.css"/>
</head>

<body>



<?php include_once("global/nav_global.php"); ?>

<?php
$url = 'http://feeds.bbci.co.uk/news/world/rss.xml';
$feed = simplexml_load_file($url, 'SimpleXMLIterator');
echo "<h2>" . $feed->channel->description . "</h2><ol>";

$filtered = new LimitIterator($feed->channel->item, 0, 10);
foreach ($filtered as $item) {?>
	<h4><li><a href="<?= $item->link; ?>" target="_blank"> <?= $item->title;?> </a></li></h4>

	<?php
	date_default_timezone_set('America/New_York');

	$date = new DateTime($item->pubDate);
	$date->setTimeZone(new DateTimeZone('America/New_York'));
	$offset = $date->getOffSet();
	$timezone = ($offset == -14400) ? 'EDT' : 'EST';
	echo $date->format('M j, Y, g:ia') . $timezone;
	?>

	<p><?php echo $item->desciption; ?></p>
	<?php } ?>
	</ol>


	<?php
include_once "global/footer.php";
?>

	</body>
	</HTML>