# LIS4381

## Nikhil Bosshardt

### Assignment 1 Requirments:

* Screenshot of ampps installation running (#1 above);
* Screenshot of running JDK java Hello (#2 above);
* Screenshot of running Android Studio My First App (#3 above, example below);
* git commands w/short descriptions (“Creating a New Repository” above);
* Bitbucket repo links: a) this assignment and b) the completed tutorial repos above (bitbucketstationlocations and myteamquotes).


*1. Screen Shot of ampps application running:*

![hwapp Screenshot](images/ss1.JPG)

*2. Screenshot of JDK java hello:*

![localhost Screenshot](images/ss2.JPG)

*3. Screenshot of Android Studio my first app:*

![localhost Screenshot](images/ss3.JPG)

*4. Git commands w/short descriptions:*

* git init - Creates an empty git repo.
* git status - Shows the current repo status, what you have and have not sent etc.
* git add - Adds a file to the index to be pushed/commited.
* git commit - Record changes to your repo.
* git push - Updates your remote repo.
* git pull - Updates your local repo.
* git rm - removes files from your index.

*5. Bitbucket Repo Links:*

1. https://bitbucket.org/ncb13/lis4381
2. Bitbucket Tutorial - Station Locations:
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/ncb13/bitbucketstationlocations/ "Bitbucket Station Locations")
3. Tutorial: Request to update a teammate's repository:
[A1 My Team Quotes Tutorial Link](https://bitbucket.org/ncb13/myteamquotes/ "My Team Quotes Tutorial")