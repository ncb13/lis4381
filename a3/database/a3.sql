-- MySQL Workbench Forward Engineering

SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='TRADITIONAL,ALLOW_INVALID_DATES';

-- -----------------------------------------------------
-- Schema localhost
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `localhost` ;

-- -----------------------------------------------------
-- Schema localhost
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `localhost` DEFAULT CHARACTER SET utf8 ;
SHOW WARNINGS;
USE `localhost` ;

-- -----------------------------------------------------
-- Table `localhost`.`petstore`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `localhost`.`petstore` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `localhost`.`petstore` (
  `pst_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_name` VARCHAR(30) NOT NULL,
  `pst_street` VARCHAR(30) NOT NULL,
  `pst_city` VARCHAR(30) NOT NULL,
  `pst_state` CHAR(2) NOT NULL,
  `pst_zip` INT(9) NOT NULL,
  `pst_phone` BIGINT NOT NULL,
  `pst_email` VARCHAR(100) NOT NULL,
  `pst_url` VARCHAR(100) NOT NULL,
  `pst_ytd_sales` DECIMAL(10,2) NOT NULL,
  `pst_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pst_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `localhost`.`customer`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `localhost`.`customer` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `localhost`.`customer` (
  `cus_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `cus_name` VARCHAR(30) NOT NULL,
  `cus_street` VARCHAR(30) NOT NULL,
  `cus_city` VARCHAR(30) NOT NULL,
  `cus_state` CHAR(2) NOT NULL,
  `cus_zip` INT(9) NOT NULL,
  `cus_phone` BIGINT NOT NULL,
  `cus_email` VARCHAR(100) NOT NULL,
  `cus_balance` DECIMAL(6,2) NOT NULL,
  `cus_total_sales` DECIMAL(6,2) NOT NULL,
  `cus_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`cus_id`))
ENGINE = InnoDB;

SHOW WARNINGS;

-- -----------------------------------------------------
-- Table `localhost`.`pet`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `localhost`.`pet` ;

SHOW WARNINGS;
CREATE TABLE IF NOT EXISTS `localhost`.`pet` (
  `pet_id` SMALLINT UNSIGNED NOT NULL AUTO_INCREMENT,
  `pst_id` SMALLINT UNSIGNED NOT NULL,
  `cus_id` SMALLINT UNSIGNED NULL,
  `pet_type` VARCHAR(45) NOT NULL,
  `pet_sex` ENUM('m', 'f') NOT NULL,
  `pet_cost` DECIMAL(6,2) NOT NULL,
  `pet_price` DECIMAL(6,2) NOT NULL,
  `pet_age` TINYINT NOT NULL,
  `pet_color` VARCHAR(30) NOT NULL,
  `pet_sale_date` DATE NULL,
  `pet_vaccine` ENUM('y', 'n') NOT NULL,
  `pet_neuter` ENUM('y', 'n') NOT NULL,
  `pet_notes` VARCHAR(255) NULL,
  PRIMARY KEY (`pet_id`),
  INDEX `fk_pet_petstore_idx` (`pst_id` ASC),
  INDEX `fk_pet_customer1_idx` (`cus_id` ASC),
  CONSTRAINT `fk_pet_petstore`
    FOREIGN KEY (`pst_id`)
    REFERENCES `localhost`.`petstore` (`pst_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION,
  CONSTRAINT `fk_pet_customer1`
    FOREIGN KEY (`cus_id`)
    REFERENCES `localhost`.`customer` (`cus_id`)
    ON DELETE NO ACTION
    ON UPDATE NO ACTION)
ENGINE = InnoDB;

SHOW WARNINGS;

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;

-- -----------------------------------------------------
-- Data for table `localhost`.`petstore`
-- -----------------------------------------------------
START TRANSACTION;
USE `localhost`;
INSERT INTO `localhost`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Carls store', '1432 Elm', 'Boca', 'FL', 123456789, 2342342345, 'ncb13@my.fsu.edu', 'google.com', 4324.90, NULL);
INSERT INTO `localhost`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Pet Center', '124 Red st. ', 'Rogers', 'CA', 346573425, 3425654636, 'alpha@omega.com', 'yahoo.com', 452.40, NULL);
INSERT INTO `localhost`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Cat Cutters', '592 Blue Ave. ', 'Katie', 'TX', 143523466, 2315365466, 'kat@kat.org', 'kat.org', 2952.90, NULL);
INSERT INTO `localhost`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Bob\'s Pets', '3534 Main st.', 'Titusville', 'GA', 351435236, 3453246456, 'dansgame@google.com', 'rogers.com', 589.90, NULL);
INSERT INTO `localhost`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'IDK Pets Co.', '999 First st.', 'Regons', 'MD', 342342564, 3426532462, 'regons@wtf.com', 'lmao.com', 593.00, NULL);
INSERT INTO `localhost`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Fish Fun Fouse', '4238 Fish Ln.', 'Fisherville', 'FL', 349508435, 3495304523, 'fishfan123@fish.com', 'fish.com', 5849.00, NULL);
INSERT INTO `localhost`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Chinchilla Co.', '234 Chinn Rd.', 'Chincilville', 'FL', 349502354, 3495234511, 'chinfan432@chin.com', 'chin.com', 349.00, NULL);
INSERT INTO `localhost`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Too lazy Co.', '482 Lazy Rd.', 'Laziville', 'GA', 239851023, 9183245342, 'lazyfan123@lazy.com', 'lazy.com', 8429.00, NULL);
INSERT INTO `localhost`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Jacobs Store', '32149 Jake St.', 'Jakeville', 'MN', 923581489, 3294589234, 'jake@jake.com', 'jake.com', 422.22, NULL);
INSERT INTO `localhost`.`petstore` (`pst_id`, `pst_name`, `pst_street`, `pst_city`, `pst_state`, `pst_zip`, `pst_phone`, `pst_email`, `pst_url`, `pst_ytd_sales`, `pst_notes`) VALUES (DEFAULT, 'Ten Pets', '10 Ten st.', 'Tensville', 'TN', 101010101, 1010101010, 'ten@ten.com', 'ten.com', 1010.10, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `localhost`.`customer`
-- -----------------------------------------------------
START TRANSACTION;
USE `localhost`;
INSERT INTO `localhost`.`customer` (`cus_id`, `cus_name`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Jake', '411 Jake St.', 'Jakeville', 'GA', 123456789, 1231231234, 'jake@jake.com', 2314.99, 2455.99, NULL);
INSERT INTO `localhost`.`customer` (`cus_id`, `cus_name`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'John', '423 John St.', 'Johnville', 'FL', 346524666, 6357458494, 'John@john.com', 256.00, 5345.66, NULL);
INSERT INTO `localhost`.`customer` (`cus_id`, `cus_name`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Ron', '542 Ron St.', 'Ronville', 'FL', 342645674, 6534886784, 'Ron@ron.com', 547.99, 345.55, NULL);
INSERT INTO `localhost`.`customer` (`cus_id`, `cus_name`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Swade', '534 Swade St.', 'Swadeville', 'CA', 346523466, 5486593457, 'swade@swade.com', 3457.66, 35.66, NULL);
INSERT INTO `localhost`.`customer` (`cus_id`, `cus_name`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Jackle', '124 Main st.', 'Jacklyville', 'CA', 236547377, 4586584568, 'jack@jack.com', 3456.77, 5.66, NULL);
INSERT INTO `localhost`.`customer` (`cus_id`, `cus_name`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Hyde', '985 Hyde Rd.', 'Hydeville', 'MN', 346534758, 5475484588, 'goog@google.com', 3756.77, 5.66, NULL);
INSERT INTO `localhost`.`customer` (`cus_id`, `cus_name`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Mark', '2534 Jdksfj St.', 'Markville', 'MN', 345756486, 5867985789, 'What@why.com', 345.88, 5.77, NULL);
INSERT INTO `localhost`.`customer` (`cus_id`, `cus_name`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Greg', '422 Greg st.', 'Gregiville', 'WA', 584547654, 3476486578, 'Ishould@haveused.com', 456.77, 3.55, NULL);
INSERT INTO `localhost`.`customer` (`cus_id`, `cus_name`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Jackie', '225 Ron st.', 'Jackieville', 'WA', 268675464, 5374574567, 'Generate@data.com', 456.77, 3.55, NULL);
INSERT INTO `localhost`.`customer` (`cus_id`, `cus_name`, `cus_street`, `cus_city`, `cus_state`, `cus_zip`, `cus_phone`, `cus_email`, `cus_balance`, `cus_total_sales`, `cus_notes`) VALUES (DEFAULT, 'Marci', 'Marci st.', 'Marciville', 'FL', 765486534, 4846846784, 'thisis@hell.com', 453.77, 2345.22, NULL);

COMMIT;


-- -----------------------------------------------------
-- Data for table `localhost`.`pet`
-- -----------------------------------------------------
START TRANSACTION;
USE `localhost`;
INSERT INTO `localhost`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 1, 1, 'Cat', 'm', 12.12, 324.76, 1, 'Blue', '1992-01-05', 'y', 'y', NULL);
INSERT INTO `localhost`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 2, 2, 'Dog', 'f', 4142.11, 42.64, 2, 'Red', '1992-01-06', 'y', 'y', NULL);
INSERT INTO `localhost`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 3, 3, 'Rat', 'm', 234.15, 23.53, 3, 'Yellow', '1992-01-07', 'y', 'y', NULL);
INSERT INTO `localhost`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 4, 4, 'Ray', 'f', 124.23, 234.55, 4, 'Green', '1992-01-10', 'y', 'y', NULL);
INSERT INTO `localhost`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 5, 5, 'Cat', 'm', 21.54, 334.55, 5, 'Blue', '1992-01-08', 'y', 'y', NULL);
INSERT INTO `localhost`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 6, 6, 'Dog', 'f', 42.65, 555.55, 6, 'Black', '1992-01-03', 'y', 'y', NULL);
INSERT INTO `localhost`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 7, 7, 'Hamster', 'm', 23.52, 34.55, 7, 'White', '1992-01-02', 'y', 'n', NULL);
INSERT INTO `localhost`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 8, 8, 'Whale', 'f', 43.65, 34.66, 1, 'Grey', '1992-01-01', 'y', 'n', NULL);
INSERT INTO `localhost`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 9, 9, 'Fish', 'm', 24.56, 345.88, 2, 'Blue', '1992-01-11', 'n', 'n', NULL);
INSERT INTO `localhost`.`pet` (`pet_id`, `pst_id`, `cus_id`, `pet_type`, `pet_sex`, `pet_cost`, `pet_price`, `pet_age`, `pet_color`, `pet_sale_date`, `pet_vaccine`, `pet_neuter`, `pet_notes`) VALUES (DEFAULT, 10, 10, 'Fish', 'f', 52.54, 234.88, 3, 'Green', '1992-01-12', 'n', 'n', NULL);

COMMIT;

