# LIS4381

## Nikhil Bosshardt

### Assignment 3 Requirments:

* Screenshot of petstore database
* Links to MWB and SQL Files
* Screenshot of concert app running
* Screenshot of concert app with calc


*1. Screen Shot of petstore database MWB:*

![hwapp Screenshot](images/ss1.PNG)

*Links:*
** [MWB Link](database/ncb13_a3.mwb) | **
** [SQL Link](database/a3.sql) **


*2. Screenshot of application running*

![localhost Screenshot](images/ss2.png)

*3. Screenshot of application doing the calculation*

![localhost Screenshot](images/ss3.png)